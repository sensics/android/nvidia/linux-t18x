/*
 * tegra_t186_amati_wm8731.c - Tegra t186 Machine driver
 *
 * Copyright (c) 2015-2017 NVIDIA CORPORATION.  All rights reserved.
 * Copyright (c) 2018 Sensics, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/input.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/i2c.h>
#include <linux/regulator/consumer.h>
#include <linux/delay.h>
#include <soc/tegra/pmc.h>
#ifdef CONFIG_SWITCH
#include <linux/switch.h>
#endif
#include <linux/pm_runtime.h>
#include <linux/version.h>
#include <linux/platform_data/tegra_asoc_pdata.h>

#include <sound/core.h>
#include <sound/jack.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include "../codecs/wm8731.h"

#include "tegra_asoc_utils_alt.h"
#include "tegra_asoc_machine_alt.h"
#include "tegra_asoc_machine_alt_t18x.h"
#include "tegra210_xbar_alt.h"

/* this portion from m3420 */
#include <linux/clk.h>
#include <linux/module.h>
#include <linux/pm_runtime.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>

#include "tegra_asoc_utils_alt.h"
#include "tegra_asoc_machine_alt.h"
#include "tegra_asoc_machine_alt_t18x.h"
#include "tegra210_xbar_alt.h"

#define DRV_NAME "tegra-snd-t186-amati-wm8731"

struct tegra_t186_amati_aud {
	struct tegra_asoc_platform_data *pdata;
	struct tegra_asoc_audio_clock_info audio_clock;
	unsigned int num_codec_links;
	int gpio_requested;
#ifdef CONFIG_SWITCH
	int jack_status;
#endif
	struct regulator *digital_reg;
	struct regulator *spk_reg;
	struct regulator *dmic_reg;
	struct snd_soc_card *pcard;
	int rate_via_kcontrol;
	int fmt_via_kcontrol;

	struct mutex lock;
	struct snd_soc_dai *i2s_master;
	unsigned int clk_ena_count;
	unsigned int srate;
	#if 0
	unsigned int i2s_master_id;
	unsigned int i2s_master_idx;
	#endif
};

static const int tegra_t186_amati_srate_values[] = {
	0,
	8000,
	16000,
	44100,
	48000,
	11025,
	22050,
	24000,
	32000,
	88200,
	96000,
	176400,
	192000,
};

#if 0

static struct snd_soc_jack tegra_t186_amati_hp_jack;

#ifdef CONFIG_SWITCH
static struct switch_dev tegra_t186_amati_headset_switch = {
        .name = "h2w",
};

static int tegra_t186_amati_jack_notifier(struct notifier_block *self,
			      unsigned long action, void *dev)
{
	struct snd_soc_jack *jack = dev;
	struct snd_soc_card *card = jack->card;
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);
	enum headset_state state = BIT_NO_HEADSET;
	int idx = 0;
	static bool button_pressed = false;

	idx = tegra_machine_get_codec_dai_link_idx_t18x("wm8731-playback");
	/* check if idx has valid number */
	if (idx == -EINVAL)
		return idx;

	dev_dbg(card->dev, "jack status = %d", jack->status);
	if (jack->status & (SND_JACK_BTN_0 | SND_JACK_BTN_1 |
		SND_JACK_BTN_2 | SND_JACK_BTN_3)) {
		button_pressed = true;
		return NOTIFY_OK;
	} else if ((jack->status & SND_JACK_HEADSET) && button_pressed) {
		button_pressed = false;
		return NOTIFY_OK;
	}

	switch (jack->status) {
	case SND_JACK_HEADPHONE:
		state = BIT_HEADSET_NO_MIC;
		break;
	case SND_JACK_HEADSET:
		state = BIT_HEADSET;
		break;
	case SND_JACK_MICROPHONE:
		/* mic: would not report */
	default:
		state = BIT_NO_HEADSET;
	}

	dev_dbg(card->dev, "switch state to %x\n", state);
	switch_set_state(&tegra_t186_amati_headset_switch, state);
	return NOTIFY_OK;
}

static struct notifier_block tegra_t186_amati_jack_detect_nb = {
	.notifier_call = tegra_t186_amati_jack_notifier,
};
#else
static struct snd_soc_jack_pin tegra_t186_amati_hp_jack_pins[] = {
	{
		.pin = "Headset Jack",
		.mask = SND_JACK_HEADSET,
	},
};
#endif
#endif

static int tegra_t186_amati_clocks_init(struct snd_soc_card *card,
					   unsigned int srate)
{
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);
	unsigned int mclk, clk_out_rate;
	int err = 0;

	/*
	 * Clocks only need to be configured once for any playback sessions
	 * and so if they are enabled, then they have been configured and
	 * we are done.
	 */
	if (machine->clk_ena_count) {
		if (machine->srate == srate)
			goto out;

		/*
		 * For multi-channel playback, the sample rates MUST be the
		 * same because the I2S master channel drives the bit-clock
		 * and frame-sync for all channels.
		 */
		dev_err(card->dev, "Multi-channel sample-rate conflict!\n");
		return -EINVAL;
	}

	switch (srate) {
	case 32000:
	case 44100:
	case 48000:
		machine->audio_clock.mclk_scale = 256;
		break;
	default:
		return -EINVAL;
	}

	if (machine->srate != srate) {
		machine->srate = srate;

		clk_out_rate = srate * machine->audio_clock.mclk_scale;
		mclk = clk_out_rate * 2;

		err = tegra_alt_asoc_utils_set_rate(&machine->audio_clock,
						    srate, mclk, clk_out_rate);
		if (err < 0) {
			dev_err(card->dev, "Can't configure clocks\n");
			return err;
		}
	}

	/*
	 * Clocks are enabled here and NOT in machine start-up because for
	 * Tegra186 we cannot the pll_a's p-divider if the pll is enabled
	 * and we cannot get the exact frequency we need. So enable it now
	 * once we have set the clock rates for the playback scenario.
	 */
	err = tegra_alt_asoc_utils_clk_enable(&machine->audio_clock);
	if (err < 0) {
		dev_err(card->dev, "Can't enable clocks\n");
		return err;
	}

	err = pm_runtime_get_sync(machine->i2s_master->dev);
	if (err < 0) {
		dev_err(card->dev, "Failed to enable I2S master\n");
		tegra_alt_asoc_utils_clk_disable(&machine->audio_clock);
		return err;
	}

out:
	machine->clk_ena_count++;

	return 0;
}

static int tegra_t186_amati_hw_params(struct snd_pcm_substream *substream,
				    struct snd_pcm_hw_params *params,
				    const char *name)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_card *card = rtd->card;
	struct snd_soc_pcm_stream *stream;
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);
	unsigned int idx, dai_fmt, bclk_ratio;
	int err;

	if (params_format(params) != SNDRV_PCM_FORMAT_S16_LE) {
		dev_err(card->dev, "Invalid data format!\n");
		return -EINVAL;
	}

	mutex_lock(&machine->lock);
	err = tegra_t186_amati_clocks_init(card, params_rate(params));
	mutex_unlock(&machine->lock);
	if (err)
		return err;

	idx = tegra_machine_get_codec_dai_link_idx_t18x(name);

	stream = (struct snd_soc_pcm_stream *)card->rtd[idx].dai_link->params;
	stream->rate_min = params_rate(params);

	bclk_ratio = tegra_machine_get_bclk_ratio_t18x(&card->rtd[idx]);
	if (bclk_ratio >= 0) {
		err = snd_soc_dai_set_bclk_ratio(card->rtd[idx].cpu_dai,
						 bclk_ratio);

		if (err < 0) {
			dev_err(card->dev, "Failed to set bclk ratio for %s\n",
				card->rtd[idx].dai_link->name);
			return err;
		}
	}

	/*
	 * For the M3420 platform one of the Tegra I2S channels, I2S1,
	 * is the bit-clock and frame-sync master and it drives all the
	 * bit-clocks and frame-syncs for all other I2S channels. Thus,
	 * for all I2S channels, apart from I2S1, both the Tegra I2S
	 * interface and the codecs are I2S slaves. By default, either
	 * the Tegra I2S interface or the codec should be a master but
	 * not both. To ensure there is no signal contention on the
	 * bit-clock and frame-sync signals force the codecs that don't
	 * interface with I2S1 to be bit-clock and frame-sync slaves.
	 */
	if (machine->i2s_master != card->rtd[idx].cpu_dai) {
		dai_fmt = card->rtd[idx].dai_link->dai_fmt;
		dai_fmt &= ~SND_SOC_DAIFMT_MASTER_MASK;
		dai_fmt |= SND_SOC_DAIFMT_CBS_CFS;
		err = snd_soc_dai_set_fmt(card->rtd[idx].codec_dai, dai_fmt);
		if (err)
			return err;
	}

	return snd_soc_dai_set_sysclk(card->rtd[idx].codec_dai, 0,
				      machine->audio_clock.clk_out_rate,
				      SND_SOC_CLOCK_IN);
}
static const char * tegra_t186_amati_i2s_linknames[4] = {
	"wm8731-playback",
	"i2s-playback-2",
	"i2s-playback-3",
	"i2s-playback-4",
};
static int tegra_t186_amati_wm8731_hw_params(struct snd_pcm_substream *substream,
					 struct snd_pcm_hw_params *params)
{
	return tegra_t186_amati_hw_params(substream, params, tegra_t186_amati_i2s_linknames[0]);
}

static void tegra_t186_amati_shutdown(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct tegra_t186_amati_aud *machine;

	machine = snd_soc_card_get_drvdata(rtd->card);

	mutex_lock(&machine->lock);

	if (machine->clk_ena_count > 0) {
		machine->clk_ena_count--;

		if (!machine->clk_ena_count) {
			pm_runtime_put_sync(machine->i2s_master->dev);
			tegra_alt_asoc_utils_clk_disable(&machine->audio_clock);
		}
	}

	mutex_unlock(&machine->lock);
}
#if 0
static int tegra_t186_amati_i2s_index(struct snd_soc_card *card, unsigned int id)
{
	if (id >= ARRAY_SIZE(tegra_t186_amati_i2s_linknames)) {
		dev_err(card->dev, "Invalid I2S master!");
		return -EINVAL;
	}

	return tegra_machine_get_codec_dai_link_idx_t18x(tegra_t186_amati_i2s_linknames[id]);
}
static int tegra_t186_amati_i2s_fmt(struct snd_soc_card *card, unsigned int idx,
				  bool is_master)
{
	unsigned int dai_fmt;

	dai_fmt = card->rtd[idx].dai_link->dai_fmt;
	dai_fmt &= ~SND_SOC_DAIFMT_MASTER_MASK;
	dai_fmt |= is_master ? SND_SOC_DAIFMT_CBM_CFM : SND_SOC_DAIFMT_CBS_CFS;

	return snd_soc_dai_set_fmt(card->rtd[idx].cpu_dai, dai_fmt);
}

static int tegra_t186_amati_i2s_config(struct snd_soc_card *card,
				     struct tegra_t186_amati_aud *machine,
				     unsigned new_master_id)
{
	int new_master_idx, ret = 0;

	mutex_lock(&machine->lock);

	if (machine->i2s_master && machine->i2s_master_id == new_master_id)
		goto out;

	if (machine->clk_ena_count > 0) {
		dev_warn(card->dev,
			 "Unable to set I2S master while audio is active\n");
		ret = -EBUSY;
		goto out;
	}

	ret = tegra_t186_amati_i2s_index(card, new_master_id);
	if (ret < 0)
		goto out;

	new_master_idx = ret;

	/* Configure the current I2S master as an I2S slave */
	if (machine->i2s_master) {
		ret = tegra_t186_amati_i2s_fmt(card, machine->i2s_master_idx,
						  false);
		if (ret)
			goto out;
	}

	/* Configure the new I2S master */
	ret = tegra_t186_amati_i2s_fmt(card, new_master_idx, true);
	if (ret) {
		tegra_t186_amati_i2s_fmt(card, machine->i2s_master_idx,
					    true);
		goto out;
	}

	machine->i2s_master_id = new_master_id;
	machine->i2s_master_idx = new_master_idx;
	machine->i2s_master = card->rtd[new_master_idx].cpu_dai;

out:
	mutex_unlock(&machine->lock);

	return ret;
}
static int tegra_t186_amati_i2s_master_get(struct snd_kcontrol *kcontrol,
					 struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_card *card = snd_kcontrol_chip(kcontrol);
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);

	ucontrol->value.integer.value[0] = machine->i2s_master_id;

	return 0;
}

static int tegra_t186_amati_i2s_master_put(struct snd_kcontrol *kcontrol,
					 struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_card *card = snd_kcontrol_chip(kcontrol);
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);
	unsigned int i2s_master_id;

	i2s_master_id = ucontrol->value.integer.value[0];

	return tegra_t186_amati_i2s_config(card, machine, i2s_master_id);
}


static const char * const tegra186_i2s_control_text[] = {
	"WM8731"
};

static const struct soc_enum tegra186_i2s_control =
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(tegra186_i2s_control_text),
		tegra186_i2s_control_text);

static const struct snd_kcontrol_new tegra_amati_controls[] = {
	SOC_ENUM_EXT("I2S MASTER", tegra186_i2s_control,
		       tegra_t186_amati_i2s_master_get,
		       tegra_t186_amati_i2s_master_put),
};
#endif

static int tegra_amati_event_hp(struct snd_soc_dapm_widget *w,
					struct snd_kcontrol *k, int event)
{
	struct snd_soc_dapm_context *dapm = w->dapm;
	struct snd_soc_card *card = dapm->card;
	dev_info(card->dev, "tegra_amati_event_hp - event = %d\n", event);
#if 0
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);
	struct tegra_asoc_platform_data *pdata = machine->pdata;

	if (!(machine->gpio_requested & GPIO_HP_MUTE))
		return 0;

	gpio_set_value_cansleep(pdata->gpio_hp_mute,
				!SND_SOC_DAPM_EVENT_ON(event));
#endif
	return 0;
}

static int tegra_amati_event_ext_mic(struct snd_soc_dapm_widget *w,
					struct snd_kcontrol *k, int event)
{
	struct snd_soc_dapm_context *dapm = w->dapm;
	struct snd_soc_card *card = dapm->card;
	dev_info(card->dev, "tegra_amati_event_ext_mic - event = %d\n", event);
#if 0
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);
	struct tegra_asoc_platform_data *pdata = machine->pdata;

	if (!(machine->gpio_requested & GPIO_EXT_MIC_EN))
		return 0;

	gpio_set_value_cansleep(pdata->gpio_ext_mic_en,
				!SND_SOC_DAPM_EVENT_ON(event));
#endif
	return 0;
}
static struct snd_soc_ops tegra_t186_amati_wm8731_ops = {
	.hw_params = tegra_t186_amati_wm8731_hw_params,
	.shutdown = tegra_t186_amati_shutdown,
};

static const struct snd_soc_dapm_widget tegra_amati_dapm_widgets[] = {

	SND_SOC_DAPM_HP("w Headphone Jack", tegra_amati_event_hp),
	SND_SOC_DAPM_MIC("w Mic Jack", tegra_amati_event_ext_mic),
#if 0
	SND_SOC_DAPM_HP("Headphone-1", NULL),
	SND_SOC_DAPM_MIC("Mic-1", NULL),
#endif
};

static int tegra_t186_amati_suspend_pre(struct snd_soc_card *card)
{
	unsigned int idx;

	/* DAPM dai link stream work for non pcm links */
	for (idx = 0; idx < card->num_rtd; idx++) {
		if (card->rtd[idx].dai_link->params)
			INIT_DELAYED_WORK(&card->rtd[idx].delayed_work, NULL);
	}

	return 0;
}

static struct snd_soc_card snd_soc_tegra_t186_amati = {
	.name = "tegra186-amati-wm8731",
	.owner = THIS_MODULE,
	.suspend_pre = tegra_t186_amati_suspend_pre,
	.dapm_widgets = tegra_amati_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(tegra_amati_dapm_widgets),
#if 0
	.controls = tegra_amati_controls,
	.num_controls = ARRAY_SIZE(tegra_amati_controls),
#endif
	.fully_routed = true,
};
#if 0
static bool codec_i2s_is_master(unsigned int dai_fmt)
{
	/*
	 * If the codec is a bit-clock and frame-sync slave,
	 * then this codec interfaces to the i2s master channel.
	 */
	if ((dai_fmt & SND_SOC_DAIFMT_MASTER_MASK) == SND_SOC_DAIFMT_CBS_CFS)
		return true;
	else
		return false;
}
#endif
static void dai_set_ops(struct snd_soc_ops *ops, unsigned int idx_start,
			unsigned int idx_end)
{
	unsigned int i;

	for (i = idx_start; i <= idx_end; i++)
		tegra_machine_set_dai_ops(i, ops);
}

static int dai_link_setup(struct platform_device *pdev)
{
	struct snd_soc_card *card = platform_get_drvdata(pdev);
	struct tegra_t186_amati_aud *machine = snd_soc_card_get_drvdata(card);
	struct snd_soc_codec_conf *tegra_machine_codec_conf = NULL;
	struct snd_soc_codec_conf *tegra186_codec_conf = NULL;
	struct snd_soc_dai_link *dai_links = NULL;
	struct snd_soc_dai_link *codec_links = NULL;
	int i, err = -ENODEV;

	/* set new codec links and conf */
	codec_links = tegra_machine_new_codec_links(pdev, codec_links,
						    &machine->num_codec_links);
	if (!codec_links)
		return err;

	tegra186_codec_conf = tegra_machine_new_codec_conf(pdev,
		tegra186_codec_conf,
		&machine->num_codec_links);
	if (!tegra186_codec_conf)
		goto err_alloc_dai_link;

	/* get the xbar dai link/codec conf structure */
	dai_links = tegra_machine_get_dai_link_t18x();
	if (!dai_links)
		goto err_alloc_codec_conf;

	tegra_machine_codec_conf = tegra_machine_get_codec_conf_t18x();
	if (!tegra_machine_codec_conf)
		goto err_alloc_codec_conf;

	/* set ADMAIF dai_ops */
	dai_set_ops(&tegra_t186_amati_wm8731_ops, TEGRA186_DAI_LINK_ADMAIF1,
		    TEGRA186_DAI_LINK_ADMAIF20);

	for (i = 0; i < machine->num_codec_links; i++) {
		if (!codec_links[i].name)
			continue;

		if (strstr(codec_links[i].name, "WM8731")) {
			dai_set_ops(&tegra_t186_amati_wm8731_ops,
				    TEGRA186_DAI_LINK_ADMAIF1,
				    TEGRA186_DAI_LINK_ADMAIF2);
		}
	}


	/* append t186 amati specific dai_links */
	card->num_links = tegra_machine_append_dai_link_t18x(codec_links,
						2 * machine->num_codec_links);
	dai_links = tegra_machine_get_dai_link_t18x();
	card->dai_link = dai_links;

	/* append t186 amati specific codec_conf */
	card->num_configs =
		tegra_machine_append_codec_conf_t18x(tegra186_codec_conf,
			machine->num_codec_links);
	tegra_machine_codec_conf = tegra_machine_get_codec_conf_t18x();
	card->codec_conf = tegra_machine_codec_conf;

	return 0;

err_alloc_codec_conf:
	tegra_machine_remove_codec_conf();
err_alloc_dai_link:
	tegra_machine_remove_dai_link();

	return err;
}

static int tegra_t186_amati_driver_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	struct snd_soc_card *card = &snd_soc_tegra_t186_amati;
	struct tegra_t186_amati_aud *machine;
	struct tegra_asoc_audio_clock_info *clocks;
	int err = 0;

	if (!np) {
		dev_err(&pdev->dev, "No device tree node found!");
		return -ENODEV;
	}

	machine = devm_kzalloc(&pdev->dev, sizeof(struct tegra_t186_amati_aud),
			       GFP_KERNEL);
	if (!machine)
		return -ENOMEM;

	card->dev = &pdev->dev;
	platform_set_drvdata(pdev, card);
	snd_soc_card_set_drvdata(card, machine);
	mutex_init(&machine->lock);
	clocks = &machine->audio_clock;

	err = snd_soc_of_parse_card_name(card, "nvidia,model");
	if (err)
		return err;

	err = snd_soc_of_parse_audio_routing(card, "nvidia,audio-routing");
	if (err)
		return err;

	if (of_property_read_u32(np, "nvidia,num-clk", &clocks->num_clk) < 0) {
		dev_err(&pdev->dev, "Missing property nvidia,num-clk\n");
		return -ENODEV;
	}

	if (of_property_read_u32_array(np, "nvidia,clk-rates",
				       clocks->clk_rates,
				       clocks->num_clk) < 0) {
		dev_err(&pdev->dev, "Missing property nvidia,clk-rates\n");
		return -ENODEV;
	}

	err = dai_link_setup(pdev);
	if (err < 0) {
		dev_err(&pdev->dev, "Failed to configured DAIs!\n");
		return err;
	}

	err = tegra_alt_asoc_utils_init(clocks, &pdev->dev, card);
	if (err) {
		dev_err(&pdev->dev, "Failed to initialize Tegra ASoC utils (%d)\n", err);
		goto err_remove_dai_link;
	}

	err = tegra_alt_asoc_utils_set_parent(&machine->audio_clock, true);

	if (err) {
		dev_err(&pdev->dev, "Failed to set parent with Tegra ASoC utils (%d)\n", err);
		goto err_fini_utils;
	}


	err = snd_soc_register_card(card);
	if (err) {
		dev_err(&pdev->dev, "snd_soc_register_card failed (%d)\n", err);
		goto err_fini_utils;
	}
#if 0
	err = tegra_t186_amati_i2s_fmt(card, 0, true);
	if (err) {
		dev_err(&pdev->dev, "tegra_t186_amati_i2s_fmt failed (%d)\n", err);
		goto err_fini_utils;
	}
#endif
	return err;

err_fini_utils:
	tegra_alt_asoc_utils_fini(&machine->audio_clock);
err_remove_dai_link:
	tegra_machine_remove_dai_link();
	tegra_machine_remove_codec_conf();

	return err;
}

static int tegra_t186_amati_driver_remove(struct platform_device *pdev)
{
	struct snd_soc_card *card = platform_get_drvdata(pdev);

	snd_soc_unregister_card(card);
	tegra_machine_remove_dai_link();
	tegra_machine_remove_codec_conf();

	return 0;
}

static const struct of_device_id tegra_t186_amati_of_match[] = {
	{ .compatible = "sensics,tegra-audio-amati-wm8731", },
	{ },
};

static struct platform_driver tegra_t186_amati_driver = {
	.driver = {
		.name = DRV_NAME,
		.owner = THIS_MODULE,
		.pm = &snd_soc_pm_ops,
		.of_match_table = tegra_t186_amati_of_match,
	},
	.probe = tegra_t186_amati_driver_probe,
	.remove = tegra_t186_amati_driver_remove,
};
module_platform_driver(tegra_t186_amati_driver);

MODULE_AUTHOR("Ryan Pavlik <ryan@sensics.com> and Jon Hunter <jonathanh@nvidia.com>");
MODULE_DESCRIPTION("Tegra186 Amati WM8731 machine ASoC driver");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:" DRV_NAME);
MODULE_DEVICE_TABLE(of, tegra_t186_amati_of_match);
